package se.experis.groupTask17;

import org.springframework.stereotype.Component;
import se.experis.groupTask17.dbhandler.*;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

/*
    rjanul created on 2019-09-26
*/
@Component
public class Init {

    @PostConstruct
    private void init(){
        System.out.println("Initializing some stuffs");
        // Drop tables
        dbDelete dbdelete = new dbDelete();
        dbdelete.dropTables();

        // Create database tables if not exist
        System.out.println("Creating database tables if they do not exist, if exist, skipp");
        dbCreateTables db = new dbCreateTables();
        db.createTableAddress();
        db.createTablePerson();
        db.createTableEmail();
        db.createTablePhone();
        db.createTableRelationship();
        dbCreate dbC = new dbCreate();

        addDataToDB();
    }

    private static void addDataToDB() {
        dbCreate db = new dbCreate();


        ArrayList<String> phoneList = new ArrayList<String>();
        ArrayList<String> phoneList2 = new ArrayList<String>();
        ArrayList<String> phoneList3 = new ArrayList<String>();

        ArrayList<String> emailList1 = new ArrayList<String>();
        ArrayList<String> emailList2 = new ArrayList<String>();
        ArrayList<String> emailList3 = new ArrayList<String>();

        emailList1.add("AndreasPoulson@gmail.com");
        phoneList.add("999999");
        db.createPerson("Andreas","Poulson", "1918-01-03", phoneList, emailList1,"Malmo", "Big Street 3", 34455);

        emailList2.add("Edis_Grudic@hotmail.com");
        phoneList2.add("+46705662332");
		phoneList2.add("+46101556690");
		db.createPerson("Edis","something", "1991/04/11", phoneList2, emailList2, "Vaxjo", "Small Street 12", 35434);

        emailList3.add("rolandas_popo@hotmail.com");
		phoneList3.add("0762083039");
		db.createPerson("Rolandas","Janulis", "200bc", phoneList3, emailList3,"Vilnius", "Vilnius Street 10", 98888);

        db.createRelationship(1,2,"Son", "Father");
        db.createRelationship(1, 3, "Son", "Father");
    }
}
