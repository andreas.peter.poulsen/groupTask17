package se.experis.groupTask17.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import se.experis.groupTask17.dbhandler.dbCreate;

@Controller("/addRelationship")
public class addRelationshipController {

    String dbResult = "";

    @GetMapping("/addRelationship")
    public String handleGet(Model model) {
        if (dbResult.equals("true")) {
            model.addAttribute("addedRelationship", "Relationship was successfully added");
        } else if (dbResult.equals("false")) {
            model.addAttribute("addedRelationship", "Something went wrong, please try again");
        }
        dbResult = "";

        return "addRelationship";
    }

    @PostMapping("/addRelationship")
    public String handlePost(String person1, String person2, String relationship1, String relationship2) {
        int pers1;
        int pers2;
        try {
            pers1 = Integer.parseInt(person1);
            pers2 = Integer.parseInt(person2);

            dbCreate dbCreate = new dbCreate();
            dbResult = dbCreate.createRelationship(pers1, pers2, relationship1, relationship2);
        } catch(NumberFormatException e) {
            System.out.println(e.getMessage());
            dbResult = "false";
        }

        return "redirect:/addRelationship";
    }
}
