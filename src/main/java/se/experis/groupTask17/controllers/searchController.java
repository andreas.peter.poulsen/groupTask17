package se.experis.groupTask17.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.context.request.WebRequest;
import se.experis.groupTask17.dbhandler.dbRead;
import se.experis.groupTask17.firstNameSearchObject;
import se.experis.groupTask17.models.*;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Controller("/search")
public class searchController {

    private String search = "";
    private String searchColumn = "";

    @GetMapping("/search")
    public String handleGet(Model model) {
        if(!search.equals("")) {
            dbRead dbRead = new dbRead();
            switch(searchColumn) {
                case "firstName":
                    searchFirstName(dbRead, model);
                    break;
                case "lastName":
                    searchLastName(dbRead, model);
                    break;
                case "email":
                    searchEmail(dbRead, model);
                    break;
            }
            search = "";
        }
        return "search";
    }

    @PostMapping("/search")
    public String handlePost(String searchTerm, String searchColumn) {
        search = searchTerm;
        this.searchColumn = searchColumn;
        return "redirect:/search";
    }


    public void searchFirstName(dbRead dbRead, Model model){
        ArrayList<Person> person = dbRead.readFromPersonIfContains("firstName", search);
        ArrayList<String> personReturn = new ArrayList<String>();

        String infoToSend = "";
        for (int i = 0; i < person.size(); i++) {
            String personString = "";
            String addressString = "";
            String phoneString = "";
            String emailString = "";
            String relationshipString = "";

            personString = "Firstname: " + person.get(i).getFirstName() + ", Lastname: " + person.get(i).getLastName() + ", Born: " + person.get(i).getDateOfBirth() + "<br>";

            Address addr = dbRead.readAllFromOneAddress(person.get(i).getAddressId());
            addressString = "City: " + addr.getCity() + ", Address: " + addr.getAddress() + ", Postal: " + addr.getPostalCode() + "<br>";

            ArrayList<Phone> phoneList = dbRead.readFromPhone(person.get(i).getPersonId());
            for (Phone ph: phoneList) {
                phoneString = phoneString + "Phonenumber: " + ph.getPhoneNumber() + "<br>";
            }

            ArrayList<Email> emailList = dbRead.readFromOneEmail(person.get(i).getPersonId());
            for (Email em: emailList) {
                emailString = emailString + "Email: " + em.getEmail() + "<br>";
            }


            ArrayList<Relationship> relationshipList = dbRead.readFromRelationship(person.get(i).getPersonId());
            for (Relationship rel: relationshipList) {
                if(rel.getPersonId2() != person.get(i).getPersonId()) {
                    relationshipString = relationshipString + "Relationship: " + dbRead.readAllFromOnePerson(rel.getPersonId2()).getFirstName() + " is your " + rel.getRelationship() + "<br>";
                }
            }

            infoToSend = personString + addressString + phoneString + emailString + relationshipString + "<br>";
            personReturn.add(infoToSend);
        }

        model.addAttribute("person", personReturn);
    }

    public void searchLastName(dbRead dbRead, Model model) {
        ArrayList<Person> person = dbRead.readFromPersonIfContains("lastName", search);
        ArrayList<String> personReturn = new ArrayList<String>();

        String infoToSend = "";
        for (int i = 0; i < person.size(); i++) {
            String personString = "";
            String addressString = "";
            String phoneString = "";
            String emailString = "";
            String relationshipString = "";

            personString = "Firstname: " + person.get(i).getFirstName() + ", Lastname: " + person.get(i).getLastName() + ", Born: " + person.get(i).getDateOfBirth() + "<br>";

            Address addr = dbRead.readAllFromOneAddress(person.get(i).getAddressId());
            addressString = "City: " + addr.getCity() + ", Address: " + addr.getAddress() + ", Postal: " + addr.getPostalCode() + "<br>";

            ArrayList<Phone> phoneList = dbRead.readFromPhone(person.get(i).getPersonId());
            for (Phone ph: phoneList) {
                phoneString = phoneString + "Phonenumber: " + ph.getPhoneNumber() + "<br>";
            }

            ArrayList<Email> emailList = dbRead.readFromOneEmail(person.get(i).getPersonId());
            for (Email em: emailList) {
                emailString = emailString + "Email: " + em.getEmail() + "<br>";
            }


            ArrayList<Relationship> relationshipList = dbRead.readFromRelationship(person.get(i).getPersonId());
            for (Relationship rel: relationshipList) {
                if(rel.getPersonId2() != person.get(i).getPersonId()) {
                    relationshipString = relationshipString + "Relationship: " + dbRead.readAllFromOnePerson(rel.getPersonId2()).getFirstName() + " is your " + rel.getRelationship() + "<br>";
                }
            }

            infoToSend = personString + addressString + phoneString + emailString + relationshipString + "<br>";
            personReturn.add(infoToSend);
        }

        model.addAttribute("person", personReturn);
    }

    public void searchEmail(dbRead dbRead, Model model) {
        ArrayList<Email> emailList = dbRead.searchFromEmail(search);
        ArrayList<String> personReturn = new ArrayList<String>();

        String infoToSend = "";
        for (int i = 0; i < emailList.size(); i++) {
            String personString = "";
            String addressString = "";
            String phoneString = "";
            String emailString = "";
            String relationshipString = "";

            Person person = dbRead.readAllFromOnePerson(emailList.get(i).getPersonId());
            personString = "Firstname: " + person.getFirstName() + ", Lastname: " + person.getLastName() + ", Born: " + person.getDateOfBirth() + "<br>";

            Address addr = dbRead.readAllFromOneAddress(person.getAddressId());
            addressString = "City: " + addr.getCity() + ", Address: " + addr.getAddress() + ", Postal: " + addr.getPostalCode() + "<br>";

            ArrayList<Phone> phoneList = dbRead.readFromPhone(person.getPersonId());
            for (Phone ph: phoneList) {
                phoneString = phoneString + "Phonenumber: " + ph.getPhoneNumber() + "<br>";
            }

            for (Email em: emailList) {
                if(em.getPersonId() == person.getPersonId()) {
                    emailString = emailString + "Email: " + em.getEmail() + "<br>";
                }
            }

            ArrayList<Relationship> relationshipList = dbRead.readFromRelationship(person.getPersonId());
            for (Relationship rel: relationshipList) {
                if(rel.getPersonId2() != person.getPersonId()) {
                    relationshipString = relationshipString + "Relationship: " + dbRead.readAllFromOnePerson(rel.getPersonId2()).getFirstName() + " is your " + rel.getRelationship() + "<br>";
                }
            }

            infoToSend = personString + addressString + phoneString + emailString + relationshipString + "<br>";
            personReturn.add(infoToSend);
        }

        model.addAttribute("person", personReturn);
    }
}
