package se.experis.groupTask17.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import se.experis.groupTask17.dbhandler.dbCreate;

import java.util.ArrayList;

@Controller("/addPerson")
public class addPersonController {

    private String firstName = "";
    private String lastName="";
    private String dateOfBirth="";
    private String phoneNumber1="";
    private String phoneNumber2="";
    private String email1 = "";
    private String email2 = "";
    private String city = "";
    private String address = "";
    private int postalCode = 0;

    @GetMapping("/addPerson")
    public String addPerson(Model model){
        if(!firstName.equals("")) {
            ArrayList<String> phoneNumbers = new ArrayList();
            phoneNumbers.add(phoneNumber1);
            phoneNumbers.add(phoneNumber2);

            ArrayList<String> emails = new ArrayList<>();
            emails.add(email1);
            emails.add(email2);

            dbCreate dbCreate = new dbCreate();

            String returnMessage = dbCreate.createPerson(firstName, lastName, dateOfBirth, phoneNumbers, emails, city, address, postalCode);

            model.addAttribute("message", returnMessage);
            // resets first name so message does not get printed if trying to add second person
            firstName = "";
        }
        return "addPerson";
    }

    @PostMapping("/addPerson")
        public String handlePost(String fName, String lName, String dOB , String pN1, String pN2, String em1, String em2, String c, String a, String pC) {
            firstName = fName;
            lastName = lName;
            dateOfBirth = dOB;
            phoneNumber1 = pN1;
            phoneNumber2 = pN2;
            email1 = em1;
            email2 = em2;
            city = c;
            address = a;
            if(!pC.equals(""))
                postalCode = Integer.parseInt(pC);
            else
                postalCode = 0;
            return "redirect:/addPerson";
    }
}
