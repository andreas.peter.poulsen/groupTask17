package se.experis.groupTask17.dbhandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class dbDelete extends dbConnection{
    /**
     *Deletes person entry
     *@param personId is int
     */
    public void deletePerson(int personId) {
        String sql = "DELETE FROM \"Person\" WHERE \"personId\" = ?";
        Connection conn = null;
        try {
            conn = connect();
            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, personId);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if(conn != null){
                    conn.close();
                }
            } catch(SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     *Deletes phone entry
     *@param phoneId is int
     */
    public void deletePhone(int phoneId) {
        String sql = "DELETE FROM \"Phone\" WHERE \"phoneId\" = ?";
        Connection conn = null;
        try {
            conn = connect();
            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, phoneId);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if(conn != null){
                    conn.close();
                }
            } catch(SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     *Deletes address entry
     *@param addressId is int
     */
    public void deleteAddress(int addressId) {
        String sql = "DELETE FROM \"Address\" WHERE \"addressId\" = ?";
        Connection conn = null;
        try {
            conn = connect();
            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, addressId);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if(conn != null){
                    conn.close();
                }
            } catch(SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     *Deletes relationship entry
     *@param relationshipId is int
     */
    public void deleteRelationship(int relationshipId) {
        String sql = "DELETE FROM \"Relationship\" WHERE \"relationshipId\" = ?";
        Connection conn = null;
        try {
            conn = connect();
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, relationshipId);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if(conn != null){
                    conn.close();
                }
            } catch(SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     *Deletes email entry
     *@param emailId is int
     */

    public void deleteEmail(int emailId) {
        String sql = "DELETE FROM \"Email\" WHERE \"emailId\" = ?";
        Connection conn = null;
        try {
            conn = connect();
            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, emailId);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if(conn != null){
                    conn.close();
                }
            } catch(SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public void dropTables(){
        String sql = "DROP Table \"Address\",\"Email\",\"Person\",\"Phone\",\"Relationship\"";
        Connection conn = null;
        try {
            conn = connect();
            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if(conn != null){
                    conn.close();
                }
            } catch(SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
