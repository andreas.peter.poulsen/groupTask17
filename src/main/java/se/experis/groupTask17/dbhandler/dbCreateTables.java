package se.experis.groupTask17.dbhandler;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/*
    rjanul created on 2019-09-26
*/
public class dbCreateTables extends dbConnection{

    public boolean createTableAddress() {
        String sql = "CREATE TABLE IF NOT EXISTS \"Address\" (\n" +
                "\t\"addressId\"\tSERIAL NOT NULL PRIMARY KEY UNIQUE,\n" +
                "\t\"city\"\tvarchar(50) NOT NULL,\n" +
                "\t\"address\"\tvarchar(50) NOT NULL,\n" +
                "\t\"postalCode\"\tINTEGER\n" +
                ")";

        Connection conn = null;
        try{
            conn = connect();
            Statement stmt  = conn.createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                return false;
            }
        }
        return true;
    }

    public boolean createTablePerson() {
        String sql = "CREATE TABLE IF NOT EXISTS \"Person\" (\n" +
                "\t\"personId\"\tSERIAL NOT NULL PRIMARY KEY UNIQUE,\n" +
                "\t\"firstName\"\tvarchar(50) NOT NULL,\n" +
                "\t\"lastName\"\tvarchar(50) NOT NULL,\n" +
                "\t\"dateOfBirth\" varchar(50) NOT NULL,\n" +
                "\t\"addressId\"\tINTEGER NOT NULL,\n" +
                "\tFOREIGN KEY(\"addressId\") REFERENCES \"Address\"(\"addressId\")\n" +
                ")";

        Connection conn = null;
        try{
            conn = connect();
            Statement stmt  = conn.createStatement();
            stmt.executeUpdate(sql);

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                return false;
            }
        }
        return true;
    }
    public boolean createTablePhone() {
        String sql = "CREATE TABLE IF NOT EXISTS \"Phone\" (\n" +
                "\t\"phoneId\"\tSERIAL NOT NULL PRIMARY KEY  UNIQUE,\n" +
                "\t\"personId\"\tINTEGER NOT NULL,\n" +
                "\t\"phoneNumber\"\tvarchar(50) NOT NULL UNIQUE,\n" +
                "\tFOREIGN KEY(\"personId\") REFERENCES \"Person\"(\"personId\")\n" +
                ")";

        Connection conn = null;
        try{
            conn = connect();
            Statement stmt  = conn.createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                return false;
            }
        }
        return true;
    }

    public boolean createTableEmail() {
        String sql = "CREATE TABLE IF NOT EXISTS \"Email\" (\n" +
                "\t\"emailId\"\tSERIAL NOT NULL PRIMARY KEY  UNIQUE,\n" +
                "\t\"personId\"\tINTEGER NOT NULL,\n" +
                "\t\"email\"\tvarchar(50) NOT NULL UNIQUE,\n" +
                "\tFOREIGN KEY(\"personId\") REFERENCES \"Person\"(\"personId\")\n" +
                ")";

        Connection conn = null;
        try{
            conn = connect();
            Statement stmt  = conn.createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                return false;
            }
        }
        return true;
    }
    public boolean createTableRelationship() {
        String sql = "CREATE TABLE IF NOT EXISTS \"Relationship\" (\n" +
                "\t\"relationshipId\"\tSERIAL NOT NULL UNIQUE,\n" +
                "\t\"personId1\"\tINTEGER NOT NULL,\n" +
                "\t\"personId2\"\tINTEGER NOT NULL,\n" +
                "\t\"relationship\"\tvarchar(50) NOT NULL,\n" +
                "\tPRIMARY KEY(\"personId2\",\"personId1\"),\n" +
                "\tFOREIGN KEY(\"personId1\") REFERENCES \"Person\"(\"personId\"),\n" +
                "\tFOREIGN KEY(\"personId2\") REFERENCES \"Person\"(\"personId\")\n" +
                ")";

        Connection conn = null;
        try{
            conn = connect();
            Statement stmt  = conn.createStatement();
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
                return false;
            }
        }
        return true;
    }
}
