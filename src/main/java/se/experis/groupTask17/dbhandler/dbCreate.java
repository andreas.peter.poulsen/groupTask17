package se.experis.groupTask17.dbhandler;

import java.sql.*;
import java.util.ArrayList;

public class dbCreate extends dbConnection{

    public String createPerson(String firstName, String lastName, String dateOfBirth, ArrayList<String> phoneNumber, ArrayList<String> email, String city, String address, int postalCode) {
        String addressSQL = "INSERT INTO \"Address\"(\"city\", \"address\", \"postalCode\")VALUES(?, ? ,?)";
        String personSQL = "INSERT INTO \"Person\"(\"firstName\", \"lastName\", \"dateOfBirth\", \"addressId\")VALUES(?, ? ,?, ?)";
        String phoneSQL = "INSERT INTO \"Phone\"(\"personId\", \"phoneNumber\")VALUES(?, ?)";
        String emailSQL = "INSERT INTO \"Email\"(\"personId\", \"email\")VALUES(?, ?)";

        Connection conn = null;
        boolean autoCommit = false;
        try {
            conn = connect();
            autoCommit = conn.getAutoCommit();
            conn.setAutoCommit(false);

            // 1. add address
            PreparedStatement pstmt = conn.prepareStatement(addressSQL, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, city);
            pstmt.setString(2, address);
            pstmt.setInt(3, postalCode);

            pstmt.executeUpdate();

            // get newly created address id
            int tempAddressId = 0;
            ResultSet generatedKeys = pstmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                tempAddressId = generatedKeys.getInt(1);
            }

            // 2. add person
            PreparedStatement pstmt2 = conn.prepareStatement(personSQL, Statement.RETURN_GENERATED_KEYS);
            pstmt2.setString(1, firstName);
            pstmt2.setString(2, lastName);
            pstmt2.setString(3, dateOfBirth);
            pstmt2.setInt(4, tempAddressId);

            pstmt2.executeUpdate();

            // get newly created person id
            int tempPersonId = 0;
            ResultSet generatedKeys2 = pstmt2.getGeneratedKeys();
            if (generatedKeys2.next()) {
                tempPersonId = generatedKeys2.getInt(1);
            }

            // 3. add phone
            PreparedStatement pstmt3 = conn.prepareStatement(phoneSQL);
            for(int i = 0; i < phoneNumber.size(); i++) {
                pstmt3.setInt(1, tempPersonId);
                pstmt3.setString(2, phoneNumber.get(i));
                pstmt3.addBatch();
            }

          pstmt3.executeBatch();

            // 4. add email
            PreparedStatement pstmt4 = conn.prepareStatement(emailSQL);
            for(int i = 0; i < email.size(); i++) {
                pstmt4.setInt(1, tempPersonId);
                pstmt4.setString(2, email.get(i));
                pstmt4.addBatch();
            }

            pstmt4.executeBatch();


            conn.commit();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            try {
                conn.rollback();
                return "Not added to database, PhoneNumber or Email address already exist in the database";
            } catch (SQLException ex) {
                ex.printStackTrace();
                return "Not added to database, PhoneNumber or Email address already exist in the database";
            }
        } finally {
            try {
                if(conn != null){
                    conn.setAutoCommit(autoCommit);
                    conn.close();
                }
            } catch(SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return "You were successfully added!";
    }

    public void createAddress(String city, String address, int postalCode) {        // Andreas
        String sql = "INSERT INTO \"Address\"(\"city\", \"address\", \"postalCode\")VALUES(?, ? ,?)";
        Connection conn = null;
        try {
            conn = connect();
            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setString(1, city);
            pstmt.setString(2, address);
            pstmt.setInt(3, postalCode);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if(conn != null){
                    conn.close();
                }
            } catch(SQLException e) {
                System.out.println(e.getMessage());
            }
        }

    }

    public void createPhone(int personId, String phoneNumber) {
        String sql = "INSERT INTO \"Phone\"(\"personId\", \"phoneNumber\")VALUES(?, ?)";
        Connection conn = null;
        try {
            conn = connect();
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, personId);
            pstmt.setString(2, phoneNumber);
            pstmt.setInt(1, personId);
            pstmt.setString(2, phoneNumber);

            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if(conn != null){
                    conn.close();
                }
            } catch(SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public String createRelationship(int personId1, int personId2, String relationship, String relationship2) {
        String sql = "INSERT INTO \"Relationship\"(\"personId1\", \"personId2\", \"relationship\") VALUES(?, ?, ?)";

        Connection conn = null;
        boolean autoCommit = false;
        try {
            conn = connect();
            autoCommit = conn.getAutoCommit();
            conn.setAutoCommit(false);

            PreparedStatement pstmt = conn.prepareStatement(sql);

            pstmt.setInt(1, personId1);
            pstmt.setInt(2, personId2);
            pstmt.setString(3, relationship);

            pstmt.executeUpdate();

            PreparedStatement pstmt2 = conn.prepareStatement(sql);

            pstmt2.setInt(1, personId2);
            pstmt2.setInt(2, personId1);
            pstmt2.setString(3, relationship2);

            pstmt2.executeUpdate();

            conn.commit();
            return "true";
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            try {
                conn.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            return "false";
        } finally {
            try {
                if(conn != null){
                    conn.setAutoCommit(autoCommit);
                    conn.close();
                }
            } catch(SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
