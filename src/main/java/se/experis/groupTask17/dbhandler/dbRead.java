package se.experis.groupTask17.dbhandler;

import se.experis.groupTask17.models.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class dbRead extends dbConnection{

    // returns all data from person table
    public ArrayList<Person> readAllFromPerson() {
        String sql = "SELECT * FROM \"Person\"";
        ArrayList<Person> personList = new ArrayList<Person>();
        Connection conn = null;
        try {
            conn = connect();
            Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                Person person = new Person(rs.getInt("personId"),
                        rs.getString("firstName"),
                        rs.getString("lastName"),
                        rs.getString("dateOfBirth"),
                        rs.getInt("addressId"));

                personList.add(person);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }
        return personList;
    }

    // searches person table according to personId
    public Person readAllFromOnePerson(int ID) {
        String sql = "SELECT * FROM \"Person\" WHERE \"personId\"="+ID;
        Connection conn = null;
        Person person = null;
        try {
            conn = connect();
            Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            while(rs.next()) {
                person = new Person(rs.getInt("personId"),
                        rs.getString("firstName"),
                        rs.getString("lastName"),
                        rs.getString("dateOfBirth"),
                        rs.getInt("addressId"));

            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }
        return person;
    }

    // searches person table according to searchColumn if it contains searchTerm
    public ArrayList<Person> readFromPersonIfContains(String searchColumn, String searchTerm) {
        String sql = "SELECT * FROM \"Person\" WHERE \"" + searchColumn + "\" ILIKE '%" + searchTerm +"%'";
        ArrayList<Person> personList = new ArrayList<Person>();
        Connection conn = null;
        try {
            conn = connect();
            Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                Person person = new Person(rs.getInt("personId"),
                        rs.getString("firstName"),
                        rs.getString("lastName"),
                        rs.getString("dateOfBirth"),
                        rs.getInt("addressId"));
                personList.add(person);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }
        return personList;
    }

    // returns all data from address table
    public ArrayList<Address> readAllFromAddress() {
        String sql = "SELECT * FROM \"Address\"";
        ArrayList<Address> addressList = new ArrayList<Address>();
        Connection conn = null;
        try {
            conn = connect();
            Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                Address address = new Address(rs.getInt("addressId"),
                        rs.getString("city"),
                        rs.getString("address"),
                        rs.getInt("postalCode"));

                addressList.add(address);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }
        return addressList;
    }

    // searches for address according to addressId
    public Address readAllFromOneAddress(int ID) {
        String sql = "SELECT * FROM \"Address\" WHERE \"addressId\"="+ID;

        Connection conn = null;
        Address address = null;
        try {
            conn = connect();
            Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            while(rs.next()) {
                address = new Address(rs.getInt("addressId"),
                        rs.getString("city"),
                        rs.getString("address"),
                        rs.getInt("postalCode"));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }
        return address;
    }

    // returns all data of relationship table
    public ArrayList<Relationship> readAllFromRelationship() {
        String sql = "SELECT * FROM \"Relationship\"";
        ArrayList<Relationship> relationshipList = new ArrayList<Relationship>();
        Connection conn = null;
        try {
            conn = connect();
            Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                Relationship relationship = new Relationship(rs.getInt("relationshipId"),
                        rs.getInt("personId1"),
                        rs.getInt("personId2"),
                        rs.getString("relationship"));

                relationshipList.add(relationship);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }
        return relationshipList;
    }

    // searches for relationship according to relationshipId
    public Relationship readAllFromOneRelationship(int ID) {
        String sql = "SELECT * FROM \"Relationship\" WHERE \"relationshipId\"="+ID;
        Connection conn = null;
        Relationship relationship = null;
        try {
            conn = connect();
            Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            while(rs.next()) {
                relationship = new Relationship(rs.getInt("relationshipId"),
                        rs.getInt("personId1"),
                        rs.getInt("personId2"),
                        rs.getString("relationship"));
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }
        return relationship;
    }

    // searches for relationship according to personId
    public ArrayList<Relationship> readFromRelationship(int personId) {
        String sql = "SELECT * FROM \"Relationship\" WHERE \"personId1\" = '" + personId + "'";
        ArrayList<Relationship> relationshipList = new ArrayList<Relationship>();
        Connection conn = null;
        try {
            conn = connect();
            Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                Relationship relationship = new Relationship(rs.getInt("relationshipId"),
                        rs.getInt("personId1"),
                        rs.getInt("personId2"),
                        rs.getString("relationship"));

                relationshipList.add(relationship);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }
        return relationshipList;
    }

    // returns all phone table data
    public ArrayList<Phone> readAllFromPhone() {
        String sql = "SELECT * FROM \"Phone\"";
        ArrayList<Phone> phoneList = new ArrayList<Phone>();
        Connection conn = null;
        try {
            conn = connect();
            Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                Phone phone = new Phone(rs.getInt("phoneId"),
                        rs.getInt("personId"),
                        rs.getString("phoneNumber"));

                phoneList.add(phone);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }
        return phoneList;
    }

    // searches phone table according to phoneId
    public Phone readAllFromOnePhone(int ID) {
        String sql = "SELECT * FROM \"Phone\" WHERE \"phoneId\"="+ID;
        Connection conn = null;
        Phone phone = null;
        try {
            conn = connect();
            Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            while(rs.next()) {
                phone = new Phone(rs.getInt("phoneId"),
                        rs.getInt("personId"),
                        rs.getString("phoneNumber"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }
        return phone;
    }

    // searches for phone table according to personId
    public ArrayList<Phone> readFromPhone(int personId) {
        String sql = "SELECT * FROM \"Phone\" WHERE \"personId\" = '" + personId + "'";
        ArrayList<Phone> phoneList = new ArrayList<Phone>();
        Connection conn = null;
        try {
            conn = connect();
            Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                Phone phone = new Phone(rs.getInt("phoneId"),
                        rs.getInt("personId"),
                        rs.getString("phoneNumber"));

                phoneList.add(phone);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }
        return phoneList;
    }
    // returns all data from address table
    public ArrayList<Email> readAllFromEmail() {
        String sql = "SELECT * FROM \"Email\"";
        ArrayList<Email> emails = new ArrayList<Email>();
        Connection conn = null;
        try {
            conn = connect();
            Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            // loop through the result set
            while (rs.next()) {
                Email email = new Email(rs.getInt("emailId"),
                        rs.getInt("personId"),
                        rs.getString("email"));
                emails.add(email);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }
        return emails;
    }

    public Email readAllFromOneEmail(int ID) {
        String sql = "SELECT * FROM \"Email\" WHERE \"emailId\"="+ID;
        Connection conn = null;
        Email email = null;
        try {
            conn = connect();
            Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            while(rs.next()) {
                email = new Email(rs.getInt("emailId"),
                        rs.getInt("personId"),
                        rs.getString("email"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }
        return email;
    }

    public ArrayList<Email> readFromOneEmail(int personId) {
        String sql = "SELECT * FROM \"Email\" WHERE \"personId\"="+personId;
        ArrayList<Email> emailList = new ArrayList<Email>();
        Connection conn = null;
        Email email = null;
        try {
            conn = connect();
            Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            while(rs.next()) {
                email = new Email(rs.getInt("emailId"),
                        rs.getInt("personId"),
                        rs.getString("email"));
                emailList.add(email);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }
        return emailList;
    }

    public ArrayList<Email> searchFromEmail(String emailSearch) {
        String sql = "SELECT * FROM \"Email\" WHERE \"email\" ILIKE '%" + emailSearch + "%'";
        ArrayList<Email> emailList = new ArrayList<Email>();
        Connection conn = null;
        Email email = null;
        try {
            conn = connect();
            Statement stmt  = conn.createStatement();
            ResultSet rs    = stmt.executeQuery(sql);

            while(rs.next()) {
                email = new Email(rs.getInt("emailId"),
                        rs.getInt("personId"),
                        rs.getString("email"));
                emailList.add(email);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch(SQLException e){
                System.out.println(e.getMessage());
            }
        }
        return emailList;
    }


}
