package se.experis.groupTask17.dbhandler;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class dbUpdate extends dbConnection{

    /**
     *Updates person entry
     *@param personId is int,
     *@param firstName is String,
     *@param lastName is String,
     *@param dateOfBirth is String,
     *@param addressId is int,
     */

    public void updatePerson(int personId, String firstName, String lastName, String dateOfBirth, int addressId) {

        String sql = "UPDATE \"Person\" " +
                "SET \"firstName\"= ' " + firstName + "', \"lastName\"= ' " + lastName+ " ', \"dateOfBirth\"= ' " + dateOfBirth + " ' , \"addressId\"= '" + +addressId +" '" +
                "WHERE \"personId\"= ' " + personId +" ' ";

        System.out.println(sql);

        Connection conn = null;
        try {
            conn = connect();
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if(conn != null){
                    conn.close();
                }
            } catch(SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**Updates phone entry
     *@param phoneId is int,
     *@param phoneNumber is String,
     */

    public void updatePhone(int phoneId, String phoneNumber) {
        String sql = "UPDATE \"Phone\" " +
                "SET \"phoneNumber\"= ' " + phoneNumber + "'" +
                "WHERE \"phoneId\"= ' " + phoneId +" ' ";

        System.out.println(sql);

        Connection conn = null;
        try {
            conn = connect();
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if(conn != null){
                    conn.close();
                }
            } catch(SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     *Updates address entry
     *@param addressId is int,
     *@param address is String,
     */

    public void updateAddress(int addressId, String address) {
        String sql = "UPDATE \"Address\" " +
                "SET \"address\"= ' " + address + "'" +
                "WHERE \"addressId\"= ' " + addressId +" ' ";

        System.out.println(sql);

        Connection conn = null;
        try {
            conn = connect();
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if(conn != null){
                    conn.close();
                }
            } catch(SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }


    /**
     *Updates email entry
     *@param emailid is int,
     *@param email is String,
     */

    public void updateEmail(int emailId, String email) {
        String sql = "UPDATE \"Email\" " +
                "SET \"email\"= ' " + email + "'" +
                "WHERE \"emailId\"= ' " + emailId +" ' ";

        System.out.println(sql);

        Connection conn = null;
        try {
            conn = connect();
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if(conn != null){
                    conn.close();
                }
            } catch(SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     *Updates relationship entry
     *@param relationshipId is int,
     *@param relationship is String,
     */
    public void updateRelationship(int relationshipId, String relationship) {
        String sql = "UPDATE \"Relationship\" " +
                "SET \"relationship\"= ' " + relationship + "'" +
                "WHERE \"relationshipId\"= ' " + relationshipId +" ' ";

        System.out.println(sql);

        Connection conn = null;
        try {
            conn = connect();
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if(conn != null){
                    conn.close();
                }
            } catch(SQLException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
