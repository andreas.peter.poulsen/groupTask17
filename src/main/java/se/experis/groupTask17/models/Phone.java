package se.experis.groupTask17.models;

/* 
    rjanul created on 2019-09-23
*/
public class Phone {
    private int phoneId;
    private int personId;
    private String phoneNumber;

    public Phone(int phoneId, int personId, String phoneNumber) {
        this.setPhoneId(phoneId);
        this.setPersonId(personId);
        this.setPhoneNumber(phoneNumber);
    }

    public int getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(int phoneId) {
        this.phoneId = phoneId;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
    * returns whole objects information as string
    */
    @Override
    public String toString() {
        return "Phone{" +
                "phoneId=" + phoneId +
                ", personId=" + personId +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
