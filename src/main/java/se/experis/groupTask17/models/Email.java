package se.experis.groupTask17.models;

public class Email {

    private int emailId;
    private int personId;
    private String email;

    public Email(int emailId, int personId, String email) {
        this.emailId = emailId;
        this.personId = personId;
        this.email = email;
    }

    public int getEmailId() {
        return emailId;
    }

    public void setEmailId(int emailId) {
        this.emailId = emailId;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Email{" +
                "emailId=" + emailId +
                ", personId=" + personId +
                ", email='" + email + '\'' +
                '}';
    }
}
