package se.experis.groupTask17.models;

/* 
    rjanul created on 2019-09-23
*/
public class Relationship {
    private int relationshipId;
    private int personId1;
    private int personId2;
    private String relationship;

    public Relationship(int relationshipId, int personId1, int personId2, String relationship) {
        this.relationshipId = relationshipId;
        this.personId1 = personId1;
        this.personId2 = personId2;
        this.relationship = relationship;
    }

    public int getRelationshipId() {
        return relationshipId;
    }

    public int getPersonId1() {
        return personId1;
    }

    public int getPersonId2() {
        return personId2;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationshipId(int relationshipId) {
        this.relationshipId = relationshipId;
    }

    public void setPersonId1(int personId1) {
        this.personId1 = personId1;
    }

    public void setPersonId2(int personId2) {
        this.personId2 = personId2;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    /**
    * returns whole objects information as string
    */
    @Override
    public String toString() {
        return "Relationship{" +
                "relationshipId=" + relationshipId +
                ", personId1=" + personId1 +
                ", personId2=" + personId2 +
                ", relationship='" + relationship + '\'' +
                '}';
    }
}
