package se.experis.groupTask17.models;

/* 
    rjanul created on 2019-09-23
*/
public class Address {
    private int addressId;
    private String city;
    private String address;
    private int postalCode;

    public Address(int addressId, String city, String address, int postalCode) {
        this.setAddressId(addressId);
        this.setCity(city);
        this.setAddress(address);
        this.setPostalCode(postalCode);
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(int postalCode) {
        this.postalCode = postalCode;
    }

    /**
    * returns whole objects information as string
    */
    @Override
    public String toString() {
        return "Address{" +
                "addressId=" + addressId +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", postalCode=" + postalCode +
                '}';
    }
}
