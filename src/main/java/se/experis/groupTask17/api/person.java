package se.experis.groupTask17.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import se.experis.groupTask17.dbhandler.dbRead;
import se.experis.groupTask17.models.Person;

import java.util.ArrayList;

@RestController
public class person {

    dbRead db = new dbRead();

    @GetMapping("/api/person")
    public ArrayList<Person> returnAllPeople() {
        return db.readAllFromPerson();
    }

    @GetMapping("/api/person/{ID}")
    public Person returnOnePerson(@PathVariable int ID){
        return db.readAllFromOnePerson(ID);
    }
}
