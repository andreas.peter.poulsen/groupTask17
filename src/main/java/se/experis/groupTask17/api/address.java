package se.experis.groupTask17.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import se.experis.groupTask17.dbhandler.dbRead;
import se.experis.groupTask17.models.Address;

import java.util.ArrayList;

@RestController
public class address {

    dbRead db = new dbRead();

    @GetMapping("/api/address")
    public ArrayList<Address> returnAllAddresses(){
        return db.readAllFromAddress();
    }

    @GetMapping("/api/address/{ID}")
    public Address returnOneAddress(@PathVariable int ID){
        return db.readAllFromOneAddress(ID);
    }

}
