package se.experis.groupTask17.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import se.experis.groupTask17.dbhandler.dbRead;
import se.experis.groupTask17.models.Email;

import java.util.ArrayList;

@RestController
public class email {

    dbRead db = new dbRead();

    @GetMapping("/api/email")
    public ArrayList returnAllEmails(){
        return db.readAllFromEmail();
    }

    @GetMapping("/api/email/{ID}")
    public Email returnOneEmail(@PathVariable int ID){
        return db.readAllFromOneEmail(ID);
    }
}
