package se.experis.groupTask17.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import se.experis.groupTask17.dbhandler.dbRead;
import se.experis.groupTask17.models.Relationship;

import java.util.ArrayList;

@RestController
public class relationship {

    dbRead db = new dbRead();

    @GetMapping("/api/relationship")
    public ArrayList returnAllRelationships(){
        return db.readAllFromRelationship();
    }

    @GetMapping("/api/relationship/{ID}")
    public Relationship returnOneRelationship(@PathVariable int ID){
        return db.readAllFromOneRelationship(ID);
    }
}
