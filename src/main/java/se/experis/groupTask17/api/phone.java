package se.experis.groupTask17.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import se.experis.groupTask17.dbhandler.dbRead;
import se.experis.groupTask17.models.Phone;

import java.util.ArrayList;


@RestController
public class phone {

    dbRead db = new dbRead();

    @GetMapping("/api/phone")
    public ArrayList<Phone> returnAllPhones() {
        return db.readAllFromPhone();
    }

    @GetMapping("/api/phone/{ID}")
    public Phone returnPhone(@PathVariable int ID){
        return db.readAllFromOnePhone(ID);
    }
}
